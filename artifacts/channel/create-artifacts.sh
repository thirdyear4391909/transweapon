# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF-UNINETWORK_0/fabric-samples/bin" ] ; then
PATH="/workspaces/HLF-UNINETWORK_0/fabric-samples/bin:$PATH"
fi



chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm purchaseweapon-genesis.block purchaseweapon-channel.tx bhutanAnchors.tx russiaAnchors.tx
rm -rf ../../channel-artifacts/*


#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD



# Generate the genesis block for the University Consortium Orderer
configtxgen -outputBlock purchaseweapon-genesis.block -profile PurchasingOrdererGenesis -channelID ordererchannel

# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./purchaseweapon-channel.tx -profile PurchasingChannel -channelID purchaseweaponchannel

configtxgen -outputAnchorPeersUpdate bhutanAnchors.tx -profile PurchasingChannel -channelID purchaseweaponchannel -asOrg bhutanMSP
# configtxgen -outputAnchorPeersUpdate russiaAnchors.tx -profile PurchasingChannel -channelID purchaseweaponchannel -asOrg russiaMSP

# peer channel create -o localhost:7050 -c purchaseweaponchannel -f $CONFIG_DIRECTORY/purchaseweapon-channel.tx

# peer channel create -o localhost:7050 -c purchaseweaponchannel -f $CONFIG_DIRECTORY/purchaseweapon-channel.tx









# PATH="/workspaces/EnteroruceBlockchainAssignment1/fabric-samples/bin:$PATH"


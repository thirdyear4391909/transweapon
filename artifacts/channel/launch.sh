# Launches the Orderer
# Filedger Location specified in the YAML file.
# You may override this
export ORDERER_FILELEDGER_LOCATION=$HOME/ledgers/orderer/transweapon/ledger

# Change this to control logs verbosity
export FABRIC_LOGGING_SPEC=INFO

#### Just in case this variable is not set ###
export FABRIC_CFG_PATH=$PWD

# Launch orderer
# Adjust the following command based on your Orderer setup
# Example: orderer start --configFile <path_to_config_file>
# Replace <path_to_config_file> with the actual path to your Orderer configuration file
orderer
